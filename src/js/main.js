const dropdown = document.querySelector('.dropdown');
const dropdownMenu = document.querySelector('.dropdown_menu');

dropdown.addEventListener('mouseenter', function() {
  dropdown.classList.add('open');
});

dropdown.addEventListener('mouseleave', function() {
  dropdown.classList.remove('open');
});

dropdownMenu.addEventListener('mouseleave', function() {
  dropdown.classList.remove('open');
});