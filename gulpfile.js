const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify');
const babel = require('gulp-babel');
const browserSync = require('browser-sync').create();
const clean = require('gulp-clean');
const concat = require('gulp-concat-js');
const imagemin = require('gulp-imagemin');

// Задача для компіляції SCSS в CSS
function compileSass() {
  return gulp.src('src/scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      cascade: false
    }))
    .pipe(cleanCSS())
    .pipe(gulp.dest('dist/css'))
    .pipe(browserSync.stream());
}

// Очищення папки dist
function cleanDist() {
  return gulp.src('dist', { allowEmpty: true, read: false })
    .pipe(clean());
}

// Оптимізація та копіювання зображень
function optimizeImages() {
  return gulp.src('src/img/**/*')
    .pipe(imagemin())
    .pipe(gulp.dest('dist/img'));
}

// Конкатенація та мініфікація JS
function compileScripts() {
  return gulp.src('src/js/**/*.js')
    .pipe(concat('scripts.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('dist/js'))
    .pipe(browserSync.stream());
}

// Завдання build - запускається командою gulp build
const build = gulp.series(clean, gulp.parallel(compileSass, compileScripts, optimizeImages));

// Запуск сервера та відстеження змін файлів
function devServer() {
  // Запуск сервера
  browserSync.init({
    server: {
      baseDir: './',
      port: 3000
    }
  });

  gulp.watch('src/js/**/*.js', compileScripts);
  gulp.watch('src/scss/**/*.scss', compileSass);
  gulp.watch('index.html').on('change', browserSync.reload);
}

const dev = gulp.series(build, devServer);

exports.build = build;
exports.dev = dev;
// Завдання за замовчуванням
exports.default = gulp.series(cleanDist, compileSass,compileScripts, devServer);
